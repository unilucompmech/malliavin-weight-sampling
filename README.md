# malliavin-weight-sampling  

### Description

The Malliavin calculus suggests a methodology, called the Malliavin Weight
Sampling method, for computing the derivative of an expected quantity of a
stochastic system with respect to the parameters of its underlying stochastic
variable.  This repository shows how to apply this method to some problems
found in classical mechanics, including problems solved using numerical methods
like finite elements (FEniCS) and finite differences.

### Paper

This code reproduces the results found in the paper:

Calculating the Malliavin derivative of some stochastic mechanics problems, Hauseux P, Hale JS, Bordas SPA (2017) PLOS ONE 12(12): e0189994. https://doi.org/10.1371/journal.pone.0189994

### Citing

If you find this code useful, we would kindly ask you cite our work:

    @article{10.1371/journal.pone.0189994,
      author = {Hauseux, Paul AND Hale, Jack S. AND Bordas, St�phane P. A.},
      journal = {PLOS ONE},
      publisher = {Public Library of Science},
      title = {Calculating the Malliavin derivative of some stochastic mechanics problems},
      year = {2017},
      month = {12},
      volume = {12},
      url = {https://doi.org/10.1371/journal.pone.0189994},
      pages = {1-18},
      number = {12},
      doi = {10.1371/journal.pone.0189994}
    }

### Note

Monte Carlo estimators are inherently random processes. You should not expect
*exactly* the same answer as the paper when you run this code. The global
trends should be similar however.

### Authors

Paul Hauseux, University of Luxembourg, Luxembourg.

Jack S. Hale, University of Luxembourg, Luxembourg.

### References

TODO.

### Instructions

1. Install Docker for your platform following the instructions at
   [Docker.com](https://docker.com).

2. Clone this repository:

        git clone https://bitbucket.org/unilucompmech/malliavin-weight-sampling

3. Enter the directory of the repository, and launch the Docker container using
   the command:

        cd malliavin-weight-sampling 
        ./launch-container.sh

### Description of files

All `.py` files do sensible things when run with:

    python3 <file.py>

* `launch-container.sh`: Launch the Docker container with pre-baked environment
  to execute code.

* `kelvin-voigt-gaussian/kelvin_voigt_gaussian.py`: First example in paper,
  calculates the derivative of the average strain of a Kelvin-Voigt model at
  time t with respect to the mean of the applied stress.

* `kelvin-voigt-gaussian/plot_kelvin_voigt_gaussian.py`: Plots the above.

* `kelvin-voigt-non-gaussian/kelvin_voigt_non_gaussian.py`: Same as the above,
  but with non-Gaussian stochastic variables.

* `kelvin-voigt-non-gaussian/kelvin_voigt_non_gaussian.py`: Calculates second
  derivative.

* `kelvin-voigt-non-gaussian/plot_*.py`: Plots the results from the above two 
  files.

* `elastic-bar/elastic_bar.py`: Elastic bar with uniform body traction and
  stochastic Young's modulus. Classical derivative calculated with dolfin-adjoint. 

* `elastic-bar/elastic_bar.py`: Buckling-prone hyperelastic bar and stochastic
  Young's modulus. Classical derivative calculated with dolfin-adjoint. 

### Issues and Support

Please use the
[bugtracker](http://bitbucket.org/unilucompmech/malliavin-weight-sampling) to
report any issues.

For support or questions please email [jack.hale@uni.lu](mailto:jack.hale@uni.lu).


### License 

malliavin-weight-sampling is free software: you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or (at your
option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more
details.

You should have received a copy of the GNU Lesser General Public License along
with malliavin-weight-sampling.  If not, see <http://www.gnu.org/licenses/>.
