# Copyright (C) 2017. Paul Hauseux, Jack S. Hale.
#
# This file is part of malliavin-weight-sampling.
#
# malliavin-weight-sampling is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# malliavin-weight-sampling is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with malliavin-weight-sampling. If not, see <http://www.gnu.org/licenses/>.

from dolfin import *
from dolfin_adjoint import *

import numpy as np
import numpy.random

mesh = UnitIntervalMesh(2**8)
V = FunctionSpace(mesh, "Lagrange", 1)

def model(E):
    left  = CompiledSubDomain("near(x[0], 0.0) && on_boundary")
    c = Constant(0.0)       
    bcs = DirichletBC(V, c, left)

    u = TrialFunction(V)
    v = TestFunction(V)
    u_ = Function(V)
    f = Constant(1.0)

    a = inner(E*u.dx(0), v.dx(0))*dx
    L = inner(f, v)*dx

    # Compute solution
    solve(a == L, u_, bcs)

    return u_


def main():
    E = Constant(3.0)
    u_ = model(E)

    def Jhat(E): # the functional as a pure function of E 
        u = model(E)
        return assemble(u*dx)

    # Setup functional
    J = Functional(u_*dx)
    dJdomega = compute_gradient(J, Control(E))
    J_E = Jhat(E)
    J_omega = ReducedFunctional(J, Control(E))
    
    parameters["adjoint"]["stop_annotating"] = True 

    conv_rate = taylor_test(Jhat, Control(E), J_E, dJdomega)
    
    print("Classical derivative: {}".format(dJdomega.values()[0]))

    # Malliavin derivative
    # Stochastic Young's modulus
    alpha = 2
    beta = 2
    c = 2
    num_samples = int(1E5) 
    
    def q(omega):
        q = (beta - 1.0)/(c*(1.0 - omega)) - (alpha - 1.0)/(c*omega)
        return q 

    omegas = np.random.beta(2, 2, size=num_samples) 
    mc_evaluations = np.array([J_omega(Constant(c*(1.0 + omega)))*q(omega) for omega in omegas])

    derivative_mws = np.mean(mc_evaluations)
    derivative_analytical = 1.0 - (3.0/2.0)*np.log(2)

    error = np.abs(derivative_mws - derivative_analytical)/derivative_analytical
    print("Malliavin derivative MWS: {}".format(derivative_mws))
    print("Malliavin derivative exact: {}".format(derivative_analytical))
    print("Error: {}".format(error))


if __name__ == "__main__":
    main()
