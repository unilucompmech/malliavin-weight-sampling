# Copyright (C) 2017. Paul Hauseux, Jack S. Hale.
#
# This file is part of malliavin-weight-sampling.
#
# malliavin-weight-sampling is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# malliavin-weight-sampling is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with malliavin-weight-sampling. If not, see <http://www.gnu.org/licenses/>.

from dolfin import *
from dolfin_adjoint import *

import numpy as np
import numpy.random

parameters["form_compiler"]["cpp_optimize"] = True
parameters["form_compiler"]["representation"] = "uflacs"
parameters["form_compiler"]["cpp_optimize_flags"] = "-O3 -ffast-math -march=native"

mesh = RectangleMesh(Point(0,0), Point(0.2, 0.01), 19, 9, "right/left")
V = VectorFunctionSpace(mesh, "CG", 1)

def model(omega):    
    solver_parameters = {"nonlinear_solver": "snes",
                          "snes_solver":
                           {"linear_solver": "lu",
                            "line_search": "bt",
                            "maximum_iterations": 30,
                            "absolute_tolerance": 1e-7,
                            "error_on_nonconvergence": False,
                            "report":False
                           }
                         }
    
    left  = CompiledSubDomain("near(x[0], side) && on_boundary", side=0.0)
    right = CompiledSubDomain("near(x[0], side) && on_boundary", side=0.2)
    
    # Define functions
    du = TrialFunction(V) # Incremental displacement
    v  = TestFunction(V) # Test function
    u  = Function(V) # Displacement from previous iteration
    
    boundaries = FacetFunction("size_t", mesh)
    boundaries.set_all(0)
    left.mark(boundaries, 1)
    right.mark(boundaries, 2)

    # Surface force
    f = Constant((-0.1, 0.0))
    ds = Measure('ds', domain=mesh, subdomain_data=boundaries)
    
    # Continuation parameter
    steps = 500
    t = 0.0
    T = 1.0 
    dt = T/float(steps)
    time = Expression("t", t=t, degree=0)

    bcs1 = DirichletBC(V, Constant((0.0, 0.0)), boundaries, 1)
    bcs2 = DirichletBC(V.sub(1), Constant(0.0), boundaries, 2)
    bcs = [bcs1, bcs2]
    
    # Compressible neo-Hookean model
    I = Identity(mesh.geometry().dim())
    F = I + grad(u)
    C = F.T*F
    Ic = tr(C)
    J  = det(F)

    # Elasticity parameters
    nu = 0.3
    E = omega

    psi = (E/(2.0*(1.0 + nu))/2.0)*(Ic - 2) - E/(2.0*(1.0 + nu))*ln(J) + ((E*nu/((1.0 + nu)*(1.0 - 2.0*nu)))/2.0)*(ln(J))**2

    # Total potential energy
    Pi = psi*dx - time*dot(f, u)*ds(2) 

    # Compute first variation of Pi (directional derivative about u in the direction of v)
    F = derivative(Pi, u, v)

    # Compute Jacobian of F
    J = derivative(F, u, du)
    
    while t <= T + DOLFIN_EPS:
        time.t = t
        solve(F == 0, u, bcs, J=J, bcs=bcs, solver_parameters=solver_parameters)
        t += dt 
    
    return u


def main():
    log_mean = 11.0
    log_sigma = 2.0
    mean = np.log(log_mean/(np.sqrt(1.0 + (log_sigma**2/log_mean**2))))
    sigma = np.sqrt(np.log(1.0 + (log_sigma**2/log_mean**2)))

    E = Constant(log_mean)
    u_ = model(E)

    def Jhat(E):
        u = model(E)
        return assemble(u[1]*dx)
   
    # Setup functional
    J = Functional(u_[1]*dx)
    dJdomega = compute_gradient(J, Control(E))

    J_E = Jhat(E)
    J_omega = ReducedFunctional(J, Control(E))

    parameters["adjoint"]["stop_annotating"] = True
    
    conv_rate = taylor_test(Jhat, Control(E), J_E, dJdomega)

    def q(omega):
        q = (1.0/omega + (np.log(omega) - log_mean)/(log_sigma**2*omega))
        return q

    num_samples = int(3E3) 
    omegas = np.random.lognormal(mean, sigma, size=num_samples)
    mc_evaluations = np.array([J_omega(Constant(omega))*q(omega) for omega in omegas])
        
    derivative_mws = np.mean(mc_evaluations) 
    
    print("Classical derivative at the mean parameter: {}".format(dJdomega.values()[0]))
    print("Malliavin derivative MWS: {}".format(derivative_mws))


if __name__ == "__main__":
    main()
