# Copyright (C) 2017. Paul Hauseux, Jack S. Hale.
#
# This file is part of malliavin-weight-sampling.
#
# malliavin-weight-sampling is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# malliavin-weight-sampling is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with malliavin-weight-sampling. If not, see <http://www.gnu.org/licenses/>.

import numpy as np
import chaospy as cp

# Parameters of Kelvin-Voigt model
sigma_0 = 1.0 # mean stress
eta = 1.0 # viscosity
E = 1.0 # Young's modulus

# Timestepping parameters
T = 30.0
times = np.linspace(0.0, T, num=3000)
num_steps = len(times)
dt = T/float(num_steps)
np.savetxt("output/times.txt", times)

num_samples = 20000
dist = cp.Normal(0.0, 1.0)

# Analytical solution
analytical = (1.0 - np.exp(-E*times/eta))/E
np.savetxt("output/analytical.txt", analytical)

# Malliavin derivative numerical solution
# We store the solution at all time steps.
mws = np.zeros(num_steps)

# Initial conditions on strain and Malliavin weight.
eps = np.zeros(num_steps)
q_sigma = np.zeros(num_steps)

eps[0] = 0.
q_sigma[0] = 0.
for z in range(0, num_samples):
    omega = dist.sample(num_steps)
    # Evolve ODE in time
    for i in range(1, num_steps):
        # Explicit Euler
        eps[i] = eps[i - 1]*(1.0 - (dt*E/eta)) + sigma_0*dt/eta + omega[i]*np.sqrt(dt)/eta 
        # Malliavin weight update rule
        q_sigma[i] = q_sigma[i - 1] + np.sqrt(dt)*omega[i]
        # Sample
        mws[i] += eps[i]*q_sigma[i]

# Average
mws /= num_samples

np.savetxt("output/mws.txt", mws)
