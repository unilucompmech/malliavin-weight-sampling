# Copyright (C) 2017. Paul Hauseux, Jack S. Hale.
#
# This file is part of malliavin-weight-sampling.
#
# malliavin-weight-sampling is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# malliavin-weight-sampling is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with malliavin-weight-sampling. If not, see <http://www.gnu.org/licenses/>.

import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns

sns.set_style("whitegrid")
sns.set_palette("colorblind")
color_blind = sns.color_palette("colorblind")
fig = plt.figure(figsize=(3.5,2.8))
fig.patch.set_facecolor('white')

mws = np.loadtxt("output/mws.txt") 
analytical = np.loadtxt("output/analytical.txt")
times = np.loadtxt("output/times.txt")

plt.xlabel('$\mathrm{time (s)}$')
plt.ylabel('$\partial\mathbb{E}[\epsilon]/\partial \\sigma_0$')

plt.plot(times, mws, label="MWS", color=color_blind[0], linewidth=1.0)
plt.plot(times, analytical, label="theoretical", color=color_blind[1], linewidth=1.0)

plt.tight_layout()
plt.legend(frameon=True, loc='center right')
plt.savefig("output/kelvin-voigt-gaussian.pdf")
