# Copyright (C) 2017. Paul Hauseux, Jack S. Hale.
#
# This file is part of malliavin-weight-sampling.
#
# malliavin-weight-sampling is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# malliavin-weight-sampling is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with malliavin-weight-sampling. If not, see <http://www.gnu.org/licenses/>.

from dolfin import *
import numpy as np
import chaospy as cp
from dolfin.cpp.mesh import *
from mshr import *
import time
#set_log_active(False)
from dolfin_adjoint import *
lx = 1.0
ly = 0.5
cx = 0.5
cy = 0.25
radius = 0.1
res = 50
# Generate mesh with hole.
base = Rectangle(Point(0, 0), Point(lx, ly))
hole = Circle(Point(cx, cy), radius, 20)
mesh = generate_mesh(base - hole, res)
# Define function spaces (P2-P1)
V = VectorFunctionSpace(mesh, "CG", 2)
Q = FunctionSpace(mesh, "CG", 1)
# Define trial and test functions
u = TrialFunction(V)
p = TrialFunction(Q)
v = TestFunction(V)
q = TestFunction(Q)
nu_bar = Constant(0.015) # viscosity (mean parameter)
#Define time-dependent pressure boundary condition
p_left = Expression("2.0 + sin(3*t)", t=0.0, degree = 1)
# Define boundary conditions
noslip  = DirichletBC(V, (0, 0),"(x[1] < DOLFIN_EPS | x[1] > 0.5- DOLFIN_EPS) && on_boundary")
noslip_sphere  = DirichletBC\
    (V, (0, 0),"(pow((x[0]-0.5),2) + pow((x[1]-0.25),2) - 0.01 < DOLFIN_EPS)")
inflow  = DirichletBC(Q, p_left , "x[0] < DOLFIN_EPS")
outflow = DirichletBC(Q, 0, "x[0] > 1.0 - DOLFIN_EPS")
bcu = [noslip, noslip_sphere]
bcp = [inflow, outflow]
# ds to calculate the flux
normal = FacetNormal(mesh)
boundaries = FacetFunction("size_t", mesh)
boundaries.set_all(0)
right = CompiledSubDomain("x[0] > 1.0 - DOLFIN_EPS && on_boundary")
right.mark(boundaries, 1)
ds = Measure('ds', domain=mesh, subdomain_data=boundaries)
# Create functions
u0 = Function(V)
u1 = Function(V)
p1 = Function(Q)
# Define coefficients
k = Constant(0.01) # time increment
f = Constant((0, 0))
# Tentative velocity step
F1 = (1/k)*inner(u - u0, v)*dx + inner(grad(u0)*u0, v)*dx + \
     nu_bar*inner(grad(u), grad(v))*dx - inner(f, v)*dx
a1 = lhs(F1)
L1 = rhs(F1)
# Pressure update
a2 = inner(grad(p), grad(q))*dx
L2 = -(1/k)*div(u1)*q*dx
# Velocity update
a3 = inner(u, v)*dx
L3 = inner(u1, v)*dx - k*inner(grad(p1), v)*dx

# Assemble matrices
A1 = assemble(a1)
A2 = assemble(a2)
A3 = assemble(a3)
# Use amg preconditioner if available
prec = "amg" if has_krylov_solver_preconditioner("amg") else "default"
#Set parameter values
i = 1
t = 0.0      # Initial time
T = 1.0      # Final time
times = [t,]
adj_start_timestep()
while t < T - .5*float(k):
    #print ("t = ", t)
    # Update pressure boundary condition
    p_left.t = t
    # Compute tentative velocity step
    b1 = assemble(L1)
    [bc.apply(A1, b1) for bc in bcu]
    solve(A1, u1.vector(), b1, "gmres", "default")
    end()
    # Pressure correction
    b2 = assemble(L2)
    [bc.apply(A2, b2) for bc in bcp]
    solve(A2, p1.vector(), b2, "gmres", prec)
    end()
    # Velocity correction
    b3 = assemble(L3)
    [bc.apply(A3, b3) for bc in bcu]
    solve(A3, u1.vector(), b3, "gmres", "default")
    end()
    t = i*float(k)
    times.append(t)  
    if t < T - 0.5*float(k): 
        adj_inc_timestep(t, t > T - 0.5*float(k))
    u0.assign(u1)
    i += 1

Jform =  inner(u1, normal)*ds(1)*dt[START_TIME:FINISH_TIME]
J = Functional(Jform)
m = Control(nu_bar)

dJdm = compute_gradient(J, m) 
print("Classical derivative at the mean parameter: {}".format(float(dJdm)))