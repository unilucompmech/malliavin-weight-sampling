# Copyright (C) 2017. Paul Hauseux, Jack S. Hale.
#
# This file is part of malliavin-weight-sampling.
#
# malliavin-weight-sampling is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# malliavin-weight-sampling is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with malliavin-weight-sampling. If not, see <http://www.gnu.org/licenses/>.

import numpy as np
import numpy.random
import scipy.special as sp

# Timestepping parameters
T = 2.0
times = np.linspace(0.0, T, num=200)
num_steps = len(times)
dt = T/float(num_steps)
np.savetxt("output/second_derivative_times.txt", times)

# Parameters of Kelvin-Voigt model
sigma = 1.0 # mean stress
eta = 1.0 # mean viscosity
E = 1.0 # mean Young's modulus

# numpy lognormal requires parameters of underyling normal distribution
log_mean = 0.5
log_sigma = 0.25
mean = np.log(log_mean/(np.sqrt(1.0 + (log_sigma**2/log_mean**2))))
sigma = np.sqrt(np.log(1.0 + (log_sigma**2/log_mean**2)))

# Analytical solution from Maple [time, value]
analytical =   np.array([[0., 0.], 
                         [1./10, 0.2980470834e-3], 
                         [2./10, 0.2134558847e-2], 
                         [3./10, 0.6457145714e-2], 
                         [4./10, 0.1373534463e-1], 
                         [5./10, 0.2410324267e-1], 
                         [6./10, 0.3746687806e-1], 
                         [7./10, 0.5358437160e-1], 
                         [8./10, 0.7212502179e-1], 
                         [9./10, 0.9271224302e-1], 
                         [10./10, .1149541606], 
                         [11./10, .1384648371], 
                         [12./10, .1628784447], 
                         [13./10, .1878581774], 
                         [14./10, .2131012911], 
                         [15./10, .2383413373], 
                         [16./10, .2633484064], 
                         [17./10, .2879280009], 
                         [18./10, .3119190049], 
                         [19./10, .3351910984], 
                         [20./10, .3576418733]])

np.savetxt("output/second_derivative_analytical.txt", analytical)

eps = np.zeros(num_steps)
E_sigma = np.zeros(num_steps)
E_sigma2 = np.zeros(num_steps)
E_sigma1 = np.zeros(num_steps)
E_sigmaT = np.zeros(num_steps)

# For proper convergence you need a lot of rmse runs and a lot
# of samples. Try 10 runs with 1E6 samples.
num_rmse = 5 
num_samples = 100000 

mws_rmse = np.zeros(num_steps)
for n in range(0, num_rmse):
    mws = np.zeros(num_steps) 
    for z in range(0, num_samples):
        omega = numpy.random.lognormal(mean, sigma, size=num_samples) 
        for i in range(1, num_steps):
            eps[i] = eps[i-1]*(1.-dt*E/eta)+dt*sigma/eta-np.sqrt(dt)*(omega[i])*eps[i-1]/eta-\
                     eps[i-1]*log_mean*(dt-np.sqrt(dt))/eta
            E_sigma[i] =  E_sigma[i-1] \
                          + np.sqrt(dt)/omega[i] \
                          + np.sqrt(dt)*(np.log(omega[i]) - mean)/(sigma**2*omega[i])
            E_sigma2[i] = E_sigma2[i-1] \
                          + dt*(((-1.-(np.log(omega[i]) - mean)/sigma**2 + \
                                1.0/sigma**2)*(1.0/omega[i]**2)) - \
                                (1.0 + 1.0/sigma**2)*np.exp(2.0*sigma**2 - 2.0*mean))
            E_sigmaT[i] = E_sigma[i]**2 \
                          - E_sigma2[i] \
                          - dt*i*np.exp(2.0*sigma**2 - 2.0*mean)*(1.0 + 1.0/sigma**2)
         
        mws += E_sigmaT*eps/num_samples
    
    mws_rmse += mws/num_rmse

np.savetxt("output/second_derivative_mws.txt", mws_rmse)
