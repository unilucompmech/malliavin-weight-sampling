# Copyright (C) 2017. Paul Hauseux, Jack S. Hale.
#
# This file is part of malliavin-weight-sampling.
#
# malliavin-weight-sampling is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# malliavin-weight-sampling is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with malliavin-weight-sampling. If not, see <http://www.gnu.org/licenses/>.

import numpy as np
import numpy.random
import scipy.special as sp

from progressbar import ProgressBar

# Timestepping parameters
T = 4.0
times = np.linspace(0.0, T, num=400)
num_steps = len(times)
dt = T/float(num_steps)
np.savetxt("output/times.txt", times)

# Parameters of Kelvin-Voigt model
sigma = 1.0 # mean stress
eta = 1.0 # mean viscosity
E = 1.0 # mean Young's modulus

num_samples = int(1E5) 
alpha = beta = 2.0

# stochastic variable sigma
def eps_sigma(eps, omega):
    eps_new = eps*(1.0 - (dt*E/eta)) + sigma*dt/eta + omega*np.sqrt(dt)/eta \
              + (alpha/(alpha + beta))*(dt - np.sqrt(dt))/eta
    return eps_new

def sigma_analytical(times):
    analytical = (1.0 - np.exp(-E*times/eta))/E
    return analytical


# stochastic variable eta
def eps_eta(eps, omega):
    eps_new = eps + dt*(sigma - E*eps)/(omega + eta)
    return eps_new


def eta_analytical(times):
    analytical = 6*np.exp(-times)*(-times*np.exp(times)*(times+3.)*sp.expn(1., (1./2)*times)+(2.*times+2.)*np.exp((1./2)*times)+times*np.exp(times)*(times+3.)*sp.expn(1., times)-times-2.)
    return analytical


# stochastic variable E 
def eps_E(eps, omega):
    eps_new = eps*(1.0 - (dt*E/eta)) + sigma*dt/eta - omega*np.sqrt(dt)*eps/eta \
              - eps*(alpha/(alpha + beta))*(dt - np.sqrt(dt))/eta
    return eps_new


def E_analytical(times):
    analytical = -(6.*(3.*np.log(2)*times*np.exp(2*times)-3*sp.expn(1, times)*np.exp(2*times)*times+3*np.exp(2*times)*sp.expn(1, 2*times)*times-2*times*np.exp(2*times)+2*np.exp(times)-2))*np.exp(-2*times)/times
    analytical[0] = 0.0
    return analytical


# Updating rule for Malliavin weight
def q_omega_1(q, omega):
    q_new = q + ((beta - 1.0)*np.sqrt(dt)/(1.0 - omega)) \
            - ((alpha - 1.0)*np.sqrt(dt)/omega)
    return q_new


def q_omega_2(q, omega):
    q_new = q + ((beta - 1.0)/(1.0 - omega)) \
            - ((alpha - 1.0)/omega)
    return q_new


def mws(eps_omega, q_omega):
    # Malliavin derivative numerical solution
    # We store the solution at all time steps.
    mws = np.zeros(num_steps)

    # Initial conditions on strain and Malliavin weight.
    eps = np.zeros(num_steps)
    q = np.zeros(num_steps)

    with ProgressBar(max_value=num_samples) as progress:
        for z in range(0, num_samples):
            omega = numpy.random.beta(alpha, beta, size=num_steps) 
            # Evolve ODE in time
            for i in range(1, num_steps):
                # Explicit Euler
                eps[i] = eps_omega(eps[i - 1], omega[i]) 
                # Malliavin weight update rule
                q[i] = q_omega(q[i - 1], omega[i])
                # Sample
                mws[i] += eps[i]*q[i]

            progress.update(progress.value + 1)
             

    # Average 
    mws /= num_samples

    return mws


def main():
    # analytical solutions
    sigma = sigma_analytical(times) 
    np.savetxt("output/sigma_analytical.txt", sigma)
    eta = eta_analytical(times) 
    np.savetxt("output/eta_analytical.txt", eta)
    E = E_analytical(times) 
    np.savetxt("output/E_analytical.txt", E)
   
    rmse_samples = 10 
    sigma_mws = np.zeros_like(sigma)
    eta_mws = np.zeros_like(eta)
    E_mws = np.zeros_like(E)
   
    for _ in range(0, rmse_samples):
        sigma_mws += mws(eps_sigma, q_omega_1)
        eta_mws += mws(eps_eta, q_omega_2)
        E_mws += mws(eps_E, q_omega_1)
    
    sigma_mws /= rmse_samples
    eta_mws /= rmse_samples
    E_mws /= rmse_samples

    np.savetxt("output/sigma_mws.txt", sigma_mws)
    np.savetxt("output/eta_mws.txt", eta_mws)
    np.savetxt("output/E_mws.txt", E_mws)


if __name__ == "__main__":
    main()
