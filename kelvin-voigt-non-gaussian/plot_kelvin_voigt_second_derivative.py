import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import math
import numpy as np

sns.set_style("whitegrid")
sns.set_palette("colorblind")
color_blind = sns.color_palette("colorblind")
fig = plt.figure(figsize=(3.5,2.8))
fig.patch.set_facecolor('white')

times = np.loadtxt("output/second_derivative_times.txt")
mws = np.loadtxt("output/second_derivative_mws.txt") 
analytical = np.loadtxt("output/second_derivative_analytical.txt")

plt.plot(times, mws, label="MWS", color=color_blind[0], linewidth=1.0)
plt.plot(analytical[:, 0], analytical[:, 1], label="exact solution", color=color_blind[1], linewidth=1.0) 
plt.xlabel('$\mathrm{time (s)}$')
plt.ylabel('$\\frac{\partial^2\mathbb{E}(\epsilon)}{\partial E_0^2}$')

plt.tight_layout()
plt.legend(frameon=True, loc='upper left')
plt.savefig("output/kelvin_voigt_second_derivative.pdf")
