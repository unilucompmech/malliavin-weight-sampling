import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import math
import numpy as np

sns.set_style("whitegrid")
sns.set_palette("colorblind")
color_blind = sns.color_palette("colorblind")
fig = plt.figure(figsize=(3.5,2.8))
fig.patch.set_facecolor('white')

# load data
times = np.loadtxt("output/times.txt")
y1 = np.loadtxt("output/sigma_mws.txt")
y2 = np.loadtxt("output/sigma_analytical.txt")
y3 = np.loadtxt("output/eta_mws.txt")
y4 = np.loadtxt("output/eta_analytical.txt")
y5 = np.loadtxt("output/E_mws.txt")
y6 = np.loadtxt("output/E_analytical.txt")

plt.plot(times, y1, label = "MWS", color = color_blind[0], linewidth= 1.0)
plt.plot(times, y2, label = "analytical", color = color_blind[1], linewidth= 1.0)

plt.plot(times, y3, color = color_blind[0], linewidth= 1.0)
plt.plot(times, y4, color = color_blind[1], linewidth= 1.0)

plt.plot(times, y5, color = color_blind[0], linewidth= 1.0)
plt.plot(times, y6, color = color_blind[1], linewidth= 1.0)

plt.xlabel('$\mathrm{time (s)}$')
plt.text(0.5, 0.85, '$\partial\mathbb{E}[\epsilon]/\partial\\sigma_0$', fontsize = 10)
plt.text(2.8, -0.3, '$\partial\mathbb{E}[\epsilon]/\partial\\eta_0$', fontsize = 10)
plt.text(2.0, -0.55, '$\partial\mathbb{E}[\epsilon]/\partial E_0$', fontsize = 10)

plt.tight_layout()
plt.legend(frameon=True, loc='center right')
plt.savefig("output/kelvin_voigt_non_gaussian.pdf")
